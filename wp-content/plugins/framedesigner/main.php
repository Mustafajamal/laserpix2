<?php
/**
 * Plugin Name:       Frame Designer Tool
 * Plugin URI:        logicsbuffer.com/
 * Description:       Chop Board Designer Tool Maker [frametool]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       frame-tool
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_frame_designer' );

function custom_frame_designer() {

	//add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
	add_shortcode( 'frametool', 'custom_framedesigner_form_single_chop' );
	add_action( 'wp_enqueue_scripts', 'custom_frame_designer_script' );
	//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
	//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
	// Setup Ajax action hook
	add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_read_me_later',  'read_me_later' );
	add_action( 'wp_ajax_nopriv_read_me_later', 'read_me_later' );

	//wp_localize_script( 'my_voter_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));        
	//wp_enqueue_script( 'my_voter_script' );

}

function custom_frame_designer_script() {
		        
	wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/framedesigner/js/mainscript.js',array(),time());	
	wp_enqueue_script( 'rt_custom_fancy_font_observer', plugins_url().'/framedesigner/js/fontfaceobserver.js',array(),time());	
	
	//wp_enqueue_script( 'rt_fabric_script', plugins_url().'/framedesigner/js/fabric.js');
	wp_enqueue_script( 'rt_fabric_script', plugins_url().'/framedesigner/js/fabric2-4.min.js');

	wp_enqueue_script( 'rt_lightbox_script', plugins_url().'/framedesigner/js/lightbox.js');
	wp_enqueue_script( 'rt_jquery_ui', plugins_url().'/framedesigner/js/jquery-ui.js');
	wp_enqueue_script( 'rt_canvas_toblob', plugins_url().'/framedesigner/js/canvas-toBlob.js');
	wp_enqueue_script( 'rt_html2canvas', plugins_url().'/framedesigner/js/html2canvas.js');
	wp_enqueue_script( 'rt_canvas_toblob', plugins_url().'/framedesigner/js/FileSaver.min.js');
	wp_enqueue_script( 'rt_canvas_customizecontrols', plugins_url().'/framedesigner/js/customiseControls.min.js');
	//wp_enqueue_script( 'rt_fabric_script_angular', plugins_url().'/framedesigner/js/angular.min.js',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/framedesigner/css/mainstyle.css',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style_responsive', plugins_url().'/framedesigner/css/custom_fancy_responsive.css',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_jqeury_ui', plugins_url().'/framedesigner/css/jquery-ui.min.css',array(),time());
	wp_enqueue_style( 'rt_custom_lightbox_ui', plugins_url().'/framedesigner/css/lightbox.css',array(),time());
}

function custom_framedesigner_form_single_chop() {
	$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
	global $post;
	global $woocommerce;
	//$product_id = $post->ID;

	if(isset($_POST['submit_prod'])){
			
		// //$total_qty = $_POST['rt_qty'];			 
		$quantity = 1;			 
		$product_id = $post->ID;
		$rt_total_price = $_REQUEST['total_price'];
		//$selected_size = $_REQUEST['select_size'];
		$canvas_export = $_REQUEST['canvas_export'];
		//echo $rt_total_price;
		//$woocommerce->cart->add_to_cart( $product_id, $quantity ); 
		update_post_meta($product_id, 'total_price', $rt_total_price);
		//update_post_meta($product_id, 'selected_size', $selected_size);
		update_post_meta($product_id, 'canvas_export', $canvas_export);
		// $rt_total_price = $_POST['pricetotal_quote'];
		// echo $rt_total_price;
		// echo $product_id;
		   
		//Set price
		$product_id = $post->ID;    
	    //$myPrice = get_post_meta('total_price_quote');
	    $myPrice = $rt_total_price;

	    // Get the WC_Product Object instance
		$product = wc_get_product( $product_id );
		$variation_id = "";
		$variation = "";

		// Set the product active price (regular)
		//$product->set_price( $rt_total_price );
		//$product->set_regular_price( $rt_total_price ); // To be sure

		// Save product data (sync data and refresh caches)
		//$product->save();
		//Set price end

		//die();
		// Cart item data to send & save in order
		$cart_item_data = array('custom_price' => $rt_total_price);   
		// woocommerce function to add product into cart check its documentation also 
		// what we need here is only $product_id & $cart_item_data other can be default.
		WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data);
		// // Calculate totals
		WC()->cart->calculate_totals();
		// // Save cart to session
		WC()->cart->set_session();
		// // Maybe set cart cookies
		WC()->cart->maybe_set_cart_cookies();	    

	}
	ob_start();
	//$page_title = get_the_title();
	//$terms = get_the_terms( get_the_ID(), 'product_cat' );
	//$product_type = $terms[0]->slug;
	?>
	<script src="//static.filestackapi.com/filestack-js/3.x.x/filestack.min.js"></script>
	<div class="tool-container">
		<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						

			<div id="choptoolmain" class="custom_calculator single_product">

				<!-- New Layout -->
				<div class="single_product_tool_n" >	   
				    <div class="tool-left-main" style="">      		
				   		<div class="sc-bZQynM fLLchR active">
				   			<span class="sc-htpNat iJdqWN">Drehen<svg style="display: none;" width="24" height="24" viewBox="0 0 268.65 268.65" fill="currentColor"><path d="M44.2 112.9a36.2 36.2 0 0 1 2.5-72.2L32.6 54.8a6 6 0 1 0 8.2 8.8l.3-.3 22.6-22.6a6 6 0 0 0 0-8.5L40.5 9a6 6 0 1 0-8.6 8.4l.1.1L43.5 29a48.1 48.1 0 0 0-.6 95.8h.7a6 6 0 0 0 6.1-5.9 6.2 6.2 0 0 0-5.5-6zM254 153.8c-3.8-22.4-19.6-25.9-27-26.3a7 7 0 0 1-5.6-3.2 21.3 21.3 0 0 0-18.7-10.4c-2.8 0-5.3.4-7.3 1-2.9.8-6-.3-7.7-2.7-5.3-7.2-12.2-9.7-18.1-9.2-12.8 1-10.2 9.5-11.5 9.7-4 .6-33.8-60.6-39.6-64.6-14.5-10.2-23.3-1.1-24 8.1a19 19 0 0 0 .8 7.1l27.5 91.8c.9 3 1.2 6.2.6 9.3-.7 3.6-2.3 4.9-5.3 5.5-2.2.4-31.4-31.7-42.6-36.1-2.3-.9-4.7-1.4-7.2-1.4-15.4 0-24.1 20.3-7 31.3 19.6 12.6 40 42.3 49.7 56.6 6 8.8 18.5 34.2 23.4 44a7.3 7.3 0 0 0 6.5 4.2h77c3.7-.1 10.8 0 11.2-7.1l.1-30c.1-1.1.5-2.2 1.1-3.1 21.2-32.2 27.3-53.2 23.7-74.5z"></path></svg></span>
							<button id="angle_plus" name="plus" type="button" class="sc-bxivhb kYZAvs" aria-label="button" data-uw-rm-empty-ctrl=""><span class="sc-gzVnrw httHWF"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></span></button>
							<div class="sc-ifAKCX eCcSkK">
							<input id="angleinput" type="number" maxlength="3" min="-360" max="360" class="sc-EHOje hCCPVN" value="-3" aria-label="number" data-uw-rm-form="fx"><span>°</span></div>
							<button id="angle_minus" name="minus" type="button" class="sc-bxivhb kYZAvs" aria-label="button" data-uw-rm-empty-ctrl="" data-uw-styling-context="true" style="transition: all 0s ease 0s;"><span class="sc-gzVnrw httHWF"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></span></button>
						</div>
						<div id="zoomcontroller" class="sc-bZQynM fLLchR">
							<span class="sc-htpNat iJdqWN">Zoom<svg style="display: none;" width="24" height="24" viewBox="0 0 238.06 238.06" fill="currentColor"><path d="M237 120c-4-22-20-26-27-26a7 7 0 0 1-6-3c-5-9-12-11-18-11l-8 1a7 7 0 0 1-7-3c-6-7-12-9-18-9l-5 1a7 7 0 0 1-8-8V20c0-11-8-17-16-17s-17 6-17 17v105a14 14 0 0 1-21 12l-32-14a20 20 0 0 0-7-1c-15 0-24 20-7 31 20 13 44 19 54 34 6 9 19 34 23 44a7 7 0 0 0 7 4h77a7 7 0 0 0 7-7l4-30a8 8 0 0 1 1-3c22-33 28-54 24-75z"></path><path d="M48 85a6 6 0 0 0-6-6H21l54-54v21a6 6 0 0 0 12 0V10c0-3-2-6-6-6H46a6 6 0 0 0 0 12h21L12 71V50a6 6 0 0 0-12 0v35c0 3 3 6 6 6h36a6 6 0 0 0 6-6z"></path></svg>
							</span>
							<button id="zoomplus" name="zoomplus" type="button" class="sc-bxivhb kYZAvs" aria-label="button" data-uw-rm-empty-ctrl=""><span class="sc-gzVnrw httHWF"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></span>
							</button>
								<div class="sc-ifAKCX eCcSkK">
									<input id="zoominput" type="text" min="-300" max="300" maxlength="4" class="sc-EHOje hCCPVN" value="3" novalidate><span>%</span>
								</div>
							<button id="zoomminus" name="zoomminus" type="button" class="sc-bxivhb kYZAvs" aria-label="button" data-uw-rm-empty-ctrl=""><span class="sc-gzVnrw httHWF"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg></span>
							</button>
						</div>
				   		<canvas id="canvas" width="472" height="460"></canvas>
					</div>
					<div class="tool-right">      	      	
				        <div class="main-actions-parent">
					      	<!-- //Upload Image Button -->
								<a class="upload-image-button tool-buttons main-actions">
								 <span class="tool-buttons-icon">
								    <svg width="35.943" height="54.916" viewBox="0 0 35.943 54.916" fill="currentColor"><path d="M14.29.1A1.23 1.23 0 0 1 16 1.15c.05 1.92.05 3.85 0 5.78a1.24 1.24 0 0 1-2.15.63c-.39-.36-.27-.93-.3-1.4V1.36a1.2 1.2 0 0 1 .74-1.25zM4.72 3.38c.84-.4 1.52.35 2.04.9C7.9 5.5 9.18 6.6 10.26 7.87a1.23 1.23 0 0 1-1.87 1.5A162.1 162.1 0 0 1 4.33 5.3a1.26 1.26 0 0 1 .4-1.92zM23.77 3.39a1.27 1.27 0 0 1 1.76 1.05c.04.73-.63 1.17-1.06 1.65-1.15 1.1-2.22 2.28-3.4 3.34a1.24 1.24 0 0 1-1.9-1.3 5.72 5.72 0 0 1 1.32-1.55c1.1-1.04 2.1-2.22 3.28-3.2zM10.76 15.36a4.17 4.17 0 0 1 3.95-4.14 4.23 4.23 0 0 1 4.35 3.7c.14 1.55.01 3.1.07 4.66a4.4 4.4 0 0 1 4.94 1.9 4.86 4.86 0 0 1 4.07.06 5.46 5.46 0 0 1 1.98 2.25 4.62 4.62 0 0 1 3.43.2 4.3 4.3 0 0 1 2.38 3.97c.02 3.84 0 7.68.01 11.51a7.17 7.17 0 0 1-.55 3.32c-.93 1.84-2.01 3.6-2.93 5.45-.17 1.83.02 3.67-.08 5.5a1.32 1.32 0 0 1-1.41 1.16H14.4a1.37 1.37 0 0 1-1.55-1.2c-.1-1.36.07-2.74-.08-4.1-3.24-5-6.58-9.92-9.85-14.9a4.42 4.42 0 0 1-.47-3.55 3.8 3.8 0 0 1 3.82-2.81c1.81.08 3.16 1.42 4.48 2.5V15.36m2.72.28c-.02 6.9.03 13.8-.02 20.7a1.35 1.35 0 0 1-2.18.95c-.98-.73-.12-2.17-.89-3.03a28.44 28.44 0 0 0-2.36-2.13c-.63-.5-1.33-1.15-2.2-1.03a1.53 1.53 0 0 0-.57 2.41l9.76 14.63c.92 1.18.3 2.76.55 4.13h13.98c.2-1.53-.04-3.06.13-4.58.66-1.44 1.5-2.8 2.25-4.2.56-1.09 1.39-2.15 1.27-3.45.29-4.1.32-8.23.25-12.34a1.55 1.55 0 0 0-1.64-1.51 1.42 1.42 0 0 0-1.28 1.29c-.11 1.55.1 3.12-.1 4.67a1.35 1.35 0 0 1-2.58 0c-.14-2.3.02-4.62-.07-6.92a1.73 1.73 0 0 0-.86-1.36 1.46 1.46 0 0 0-2.05 1.08c-.1 1.66.02 3.34-.04 5.01a1.34 1.34 0 0 1-2.68.2c-.1-2.12.02-4.26-.05-6.38a1.63 1.63 0 0 0-1.28-1.64 1.4 1.4 0 0 0-1.62 1.36c-.06 2.15.02 4.3-.03 6.44a1.4 1.4 0 0 1-1.38 1.35 1.34 1.34 0 0 1-1.33-1.35c-.04-4.77 0-9.53-.02-14.3a1.6 1.6 0 0 0-1.72-1.7 1.58 1.58 0 0 0-1.23 1.7zM.54 12.37a4.39 4.39 0 0 1 1.84-.2c1.58.05 3.17-.08 4.74.06a1.23 1.23 0 0 1 0 2.35c-2.04.08-4.09.06-6.13.02a1.23 1.23 0 0 1-.45-2.23zM22.06 12.3a27.44 27.44 0 0 1 4.13-.13 7.76 7.76 0 0 1 2.69.2 1.22 1.22 0 0 1-.46 2.23c-1.96.05-3.91.03-5.87.01a1.24 1.24 0 0 1-.49-2.32z"></path></svg>
								 </span>
								 <!--<span class="tool-buttons-text">Upload a Photo</span>-->
								 <?php echo do_shortcode("[filestack media_category='34']");?>
								 <?php //echo do_shortcode("[filepicker]");?>
								</a>
						</div>
						<!--//Upload Image Content --> 
						<div class="upload-image-content1" style="display:none;">
							<span class="container-popup-close">X</span>
							<h3>Select a Picture</h3>
							<p>By uploading the image i confirm that the image* rights belongs to me.</p>		
							<?php //echo do_shortcode('[ajax-file-upload unique_identifier="my_contact_form" allowed_extensions="png,jpg,jpeg" disallow_remove_button="1" upload_button_value="upload" select_file_button_value="Attach a File"]'); ?>
													
							<div id="uploaded_images" class="uploaded_images" style="display:none;"></div>
						</div>
				    </div>
				    <div id="uploaded_items"><?php echo do_shortcode('[gallery media_category="34"]');?></div>
				</div>	
			    <div class="priceandcart">
					<div class="row" id="">
						<div class="col col-4">
							<div class="col-inner">
								<div class="add-cart-btn-main">					
									<input type="submit" name="submit_prod" class="add_to_cart_btn tool-buttons-text" value="Add to Cart" />
									<span class="tool-buttons-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24"><metadata><x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c138 79.159824, 2016/09/14-01:09:01"><rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"><rdf:description rdf:about=""></rdf:description></rdf:rdf></x:xmpmeta></metadata>
									<defs><filter id="filter" x="884" y="987" width="24" height="24" filterUnits="userSpaceOnUse"><feFlood result="flood" flood-color="#1f1f1f"></feFlood><feComposite result="composite" operator="in" in2="SourceGraphic"></feComposite><feBlend result="blend" in2="SourceGraphic"></feBlend></filter></defs><path id="Forma_1_copy" data-name="Forma 1 copy" class="cls-1" d="M907.79,992.453a0.934,0.934,0,0,0-.728-0.346H890.081l-0.069-.64,0-.021A5.129,5.129,0,0,0,884.937,987a0.938,0.938,0,0,0,0,1.875,3.249,3.249,0,0,1,3.212,2.809l1.114,10.239a2.809,2.809,0,0,0-1.654,2.56v0.05a2.814,2.814,0,0,0,2.813,2.81H890.8a2.766,2.766,0,1,0,5.237,0h4.045a2.742,2.742,0,0,0-.148.89,2.766,2.766,0,1,0,2.766-2.76H890.422a0.939,0.939,0,0,1-.938-0.94v-0.05a0.939,0.939,0,0,1,.938-0.94H902.39a4.3,4.3,0,0,0,3.891-2.62,0.937,0.937,0,1,0-1.717-.75,2.446,2.446,0,0,1-2.174,1.5H891.122l-0.837-7.689h15.624l-0.459,2.2a0.939,0.939,0,0,0,.727,1.11,0.969,0.969,0,0,0,.192.019,0.938,0.938,0,0,0,.917-0.746l0.694-3.328A0.936,0.936,0,0,0,907.79,992.453ZM902.7,1007.34a0.89,0.89,0,1,1-.891.89A0.892,0.892,0,0,1,902.7,1007.34Zm-9.281,0a0.89,0.89,0,1,1-.891.89A0.891,0.891,0,0,1,893.422,1007.34Z" transform="translate(-884 -987)"></path>
									</svg></span>
								</div>		
							</div>		
						</div>
						<div id="" class="col col-4" style="display: none;">
							<?php
								$product_id = $post->ID;
								$product = wc_get_product( $product_id );
								$product_price_woo = $product->get_regular_price();
								$background = get_field("background");
								$container_background_color = get_field("container_background_color");
								//$use_image_background = get_field("use_image_background");
								
								$field = get_field_object("use_image_background");
								$use_image_background = $field['value'];
								
								//if( in_array('Yes', $use_image_background) ) {								
									//echo "<input type='hidden' name='use_image_background' id='use_image_background' value='Yes'>";								
								//}
								// if( in_array('No', $use_image_background) ) {								
								// 	echo "<input type='hidden' name='use_image_background' id='use_image_background' value='No'>";								
								// }

								$container_background_image = get_field("container_background_image");
							?>
							<div class="col-inner">
								<div class="icon-box featured-box total_nis_btn icon-box-right text-right">
									<div class="icon-box-text last-reset">									
										<div id="text-857485485" class="text">
										<input type="hidden" name="total_price_default" id="total_price_default" value="<?php echo $product_price_woo;?>">		
										<input type="hidden" name="total_price" id="total_price" value="<?php echo $product_price_woo;?>">		
										<input type="hidden" name="background" id="background" value="<?php echo $background;?>">		
										<input type="hidden" name="container_background_color" id="container_background_color" value="<?php echo $container_background_color;?>">	
										<input type="hidden" name="use_image_background" id="use_image_background" value="<?php echo $use_image_background; ?>">
										<input type="hidden" name="container_background_image" id="container_background_image" value="<?php echo $container_background_image;?>">		
										<h3>Total <span id="total_price"><?php echo $product_price_woo;?></span></h3>		
										</div>
									</div>
								</div>
							</div>
							<img style="display: none;" id="canvastoBlob" src="">
							<input type="hidden" style="display: none;" id="canvas_export" value="" name="canvas_export">
							<input type="button" style="display: block;" id="download_canvas" value="Download canvas">
							<canvas id="canvas_ex" width="475" height="460"></canvas>
						</div>
					</div>
				</div>
		</form>
	</div>

	<?php 
		$contents = ob_get_contents();
		ob_end_clean();
		return $contents;
	}
	/**
	 * Add engraving text to cart item.
	 *
	 * @param array $cart_item_data
	 * @param int   $product_id
	 * @param int   $variation_id
	 *
	 * @return array
	 */
	function iconic_add_engraving_text_to_cart_item( $cart_item_data, $product_id, $variation_id ) {
		$engraving_text = filter_input( INPUT_POST, 'canvas_export' );
		$engraving_text = filter_input( INPUT_POST, 'ywapo_text_5[0]' );

		if ( empty( $engraving_text ) ) {
			return $cart_item_data;
		}

		$cart_item_data['iconic-engraving'] = $engraving_text;

		return $cart_item_data;
	}

	add_filter( 'woocommerce_add_cart_item_data', 'iconic_add_engraving_text_to_cart_item', 10, 3 );

	/**
	 * Display engraving text in the cart.
	 *
	 * @param array $item_data
	 * @param array $cart_item
	 *
	 * @return array
	 */
	function iconic_display_engraving_text_cart( $item_data, $cart_item ) {
		if ( empty( $cart_item['iconic-engraving'] ) ) {
			return $item_data;
		}
		//'display' => '<img src="'.$cart_item['iconic-engraving'].'">',
		$base64_img = $cart_item['iconic-engraving'];
		
		//Upload to media library
		// Upload dir.
		$title = "customized";
		$upload_dir  = wp_upload_dir();
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$img             = str_replace( 'data:image/png;base64,', '', $base64_img );
		$img             = str_replace( ' ', '+', $img );
		$decoded         = base64_decode( $img );
		$filename        = $title . '.png';
		$file_type       = 'image/png';
		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// Save the image in the uploads directory.
		$upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

		$attachment = array(
			'post_mime_type' => $file_type,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
		);

		$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );
		//return $attach_id;
		$uploaded_url = wp_get_attachment_url( $attach_id );

		$item_data[] = array(
			'key'     => __( 'Engraving', 'iconic' ),
			'value'   => wc_clean( $cart_item['iconic-engraving'] ),
			'display' => '<img src="'.$uploaded_url.'">'
		);

		return $item_data;
	}

	add_filter( 'woocommerce_get_item_data', 'iconic_display_engraving_text_cart', 10, 2 );

	/**
	 * Add engraving text to order.
	 *
	 * @param WC_Order_Item_Product $item
	 * @param string                $cart_item_key
	 * @param array                 $values
	 * @param WC_Order              $order
	 */
	function iconic_add_engraving_text_to_order_items( $item, $cart_item_key, $values, $order ) {
		if ( empty( $values['iconic-engraving'] ) ) {
			return;
		}

		$item->add_meta_data( __( 'Engraving', 'iconic' ), $values['iconic-engraving'] );
	}

	add_action( 'woocommerce_checkout_create_order_line_item', 'iconic_add_engraving_text_to_order_items', 10, 4 );

	// add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
	// add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);
	// 	function order_meta_handler($item_id, $values, $cart_item_key) {
	// 		// Allow plugins to add order item meta

	// 		$cart_session = WC()->session->get('cart');
	// 		// if($cart_session[$cart_item_key]['rt_total_price'][0]){
	// 		// wc_add_order_item_meta($item_id, "Total Price", $cart_session[$cart_item_key]['rt_total_price'][0]);
	// 		// }
	// 		if($cart_session[$cart_item_key]['selected_size'][0]){
	// 			wc_add_order_item_meta($item_id, "Selected Size", $cart_session[$cart_item_key]['selected_size'][0]);
	// 		}
	// 		if($cart_session[$cart_item_key]['canvas_export'][0]){
	// 			wc_add_order_item_meta($item_id, "Thumbnail", $cart_session[$cart_item_key]['canvas_export'][0]);
	// 		}
	// 	}
	// 	function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
	// 		global $woocommerce;
	// 		$product_id = $post->ID;
	// 		//$rt_total_price = get_post_meta($product_id, 'rt_total_price');
	// 		$selected_size = get_post_meta($product_id, 'selected_size');
	// 		$canvas_export = get_post_meta($product_id, 'canvas_export');

	// 		return $cart_item_meta;
	// 	}


	// add_action( 'woocommerce_product_meta_end', 'woocommerce_single_variation_add_to_cart_btn', 20 );
	// function woocommerce_single_variation_add_to_cart_btn(){
	//     echo do_shortcode('[frametool]');
	// }
	//Ajax
	function ajax_enqueue() {
	    wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );
	    wp_localize_script( 'ajax-script', 'my_ajax_object',
	            array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

	add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );
	add_action( 'wp_ajax_foobar', 'wp_ajax_foobar_handler' );
	add_action( 'wp_ajax_nopriv_foobar', 'wp_ajax_foobar_handler' );
	function wp_ajax_foobar_handler() {

		//'display' => '<img src="'.$cart_item['iconic-engraving'].'">',
		$base64_img = $_REQUEST['canvas_export'];
		
		//Generate rendom serial number
		// $len = 10;   // total number of numbers
		// $min = 100;  // minimum
		// $max = 999;  // maximum
		// $range = []; // initialize array
		// foreach (range(0, $len - 1) as $i) {
		//     while(in_array($num = mt_rand($min, $max), $range));
		//     $range[] = $num;
		// }

		//print_r($range);

		//Upload to media library
		// Upload dir.
		$title = "MF-".date("Y-m-d")."-".uniqid()."";
		$upload_dir  = wp_upload_dir();
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$img             = str_replace( 'data:image/png;base64,', '', $base64_img );
		$img             = str_replace( ' ', '+', $img );
		$decoded         = base64_decode( $img );
		$filename        = $title . '.png';
		$file_type       = 'image/png';
		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// Save the image in the uploads directory.
		$upload_file = file_put_contents( $upload_path . $hashed_filename, $decoded );

		$attachment = array(
			'post_mime_type' => $file_type,
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $hashed_filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
			'guid'           => $upload_dir['url'] . '/' . basename( $hashed_filename )
		);

		$attach_id = wp_insert_attachment( $attachment, $upload_dir['path'] . '/' . $hashed_filename );
		//return $attach_id;
		$uploaded_url = wp_get_attachment_url( $attach_id );

		echo $uploaded_url;
		wp_die();
	}
