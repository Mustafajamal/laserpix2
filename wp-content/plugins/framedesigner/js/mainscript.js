// New Angular code
jQuery(document).ready(function() {

	jQuery("#zoomplus,#zoomminus,#angle_plus,#angle_minus").prop('disabled', true);
	jQuery("#remove-color-distance").slider({
		range: "min",
		value: 0.82,
		min: 0,
		max: 1,
		step: 0.01,
		animate: 200,
		slide: function(event, ui) {
			console.log("Remove Color Distance"+ui.value);
			applyFilterValue(2, 'distance', ui.value);
		}
	}); 

	var canvas = new fabric.Canvas('canvas',{
		preserveObjectStacking: true,
	});
	
	//Add Image Module
	jQuery(".upload-image-content").hide();
	jQuery("#uploaded_items").hide();
	jQuery( "span.container-popup-close" ).click(function(event) {
		jQuery(this).parent().hide();
	});
	jQuery( "a.upload-image-button" ).click(function() {
	   jQuery(".upload-image-content").toggle();
	});
	var retrieved_images = localStorage.getItem("user_images_content");
	jQuery( "#uploaded_images" ).html(retrieved_images);
	// window.addEventListener( "afu_file_uploaded", function(e){
	//     if( "undefined" !== typeof e.data.response.media_uri ) {
	//         console.log( e.data.response.media_uri ); // the uploaded media URL
	//         jQuery(".upload-image-button.tool-buttons.main-actions,.upload-image-content").hide();
	//         loadImage(e.data.response.media_uri);
	//         jQuery("#uploaded_images").append("<div class='single-image'><span class='remove-image'>X</span><img width='80px' src='"+e.data.response.media_uri+"'></div>");
	//         var added_images = jQuery( "#uploaded_images" ).html();
	//         localStorage.setItem("user_images_content", added_images);
	//     }
	// }, false);	
	// Custom Control
 	fabric.Canvas.prototype.customiseControls({
 		settings: {
 			borderColor: '#0C92DF',
 			cornerSize: 20,
 			cornerShape: 'rect',
 			cornerBackgroundColor: '#0C92DF',
 			cornerPadding: 5
 		},
 		tl: {
 			action: function( e, object ) {
 				var object = fabric.util.object.clone(canvas.getActiveObject());
 				object.set("top", object.top+5);
 				object.set("left", object.left+5);
 				canvas.add(object);
 			}
 		},
 		tr: {
 			action: 'rotate',
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/rotate.svg'
 		},
 		bl: {
 			action: function( e, object ) {
 				var obj = canvas.getActiveObject();
 				console.log("obj.id.."+obj.id);
 				var removedobjid = obj.id;
 				jQuery('#'+obj.id).remove();
 				var current_id = 'cardalltexthex1';
 				canvas.remove(obj);
 				jQuery(".main-actions-parent").show();
 				canvas.renderAll();
 			},
 			cursor: 'pointer',
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/removew.svg'
 		},
 		br: {
 			action: 'scale',
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/resize.svg'
 		},
 		mb: false,
	    // only is hasRotatingPoint is not set to false
	    mtr: false,
	}, function() {
		canvas.renderAll();
	});
 	fabric.Object.prototype.customiseCornerIcons({
 		settings: {
 			borderColor: '#0C92DF',
 			cornerSize: 20,
 			cornerShape: 'rect',
 			cornerBackgroundColor: '#0C92DF',
 			cornerPadding: 5
 		},
 		tl: {
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/clone.svg'
 		},	    
 		tr: {
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/rotate.svg'
 		},
 		bl: {
 			action: 'remove',
 			cursor: 'pointer',
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/removew.svg'
 		},
 		br: {
 			icon: 'https://meinfoto-3d.com/wp-content/plugins/framedesigner/images/icons/resize.svg'
 		},
 		mb: false,
	    // only is hasRotatingPoint is not set to false
	    mtr: false,
	}, function() {
		canvas.renderAll();
	});

	//Remove Image when clicked
	jQuery(document).on('click', '.single-image .remove-image', function() {
		jQuery(this).parent().remove();
	});

	//Load Image on Upload
	function loadImage(src) {
		//fabric.Image.fromURL(src, function(img) {
		fabric.util.loadImage(src, function(img) {
			var logo = new fabric.Image(img);
			//var logo = img.set({ 
			logo.set({ 
				left: 140, 
				top: 180,
				hasBorders: true,
				selectable: true,
				dragable: true,
				evented: true
			});
			//console.log("Logo.."+logo);
		    //logo.toGroup();
		    var width = logo.width; 
		    var height = logo.height;

	        if (width > 1500 || height > 1500) {
	        	alert("Max size limit is 2500x2500");
	        	jQuery(".main-actions-parent").show();
	        	return false;
		    }else{	
	        	canvas.add(logo);
	        }				
		    logo.setControlsVisibility({
		    	bl: true,
		    	br: true,
		    	tl: true,
		    	tr: true,
		    	mb: false,
		    	ml: false,
		    	mr: false,
		    	mt: false,
		    	mtr: false,
		    });
		    logo.hasRotatingPoint = true;
			logo.scaleToWidth(140);
		    logo.sendToBack();
		    logo.sendBackwards();
		    canvas.setActiveObject(logo);

		    //Apply max image limit		    
		    console.log("width.."+width); 
		    console.log("height.."+height);
		    if (width > 1500 || height > 1500) {
		    	alert("Max size limit is 2500x2500");
		    	jQuery(".main-actions-parent").show();
		    	return false;
		    }else{
		    	applyGreyScale();
		    } 
			
			canvas.renderAll();
		}, null, {crossOrigin: 'Anonymous'});
	}

		//Export Canvas
		//jQuery( "#download_canvas" ).click(function( event ) {
		//jQuery( "#add_cart" ).submit(function( event ) {
		jQuery( "#zoomplus,#zoomminus,#angle_plus,#angle_minus" ).click(function( event ) {
			// var json_data = canvas.toJSON();
			// console.log(json_data);
			// //adding 2nd canvas Data
			// // Do some initializing stuff
			// fabric.Object.prototype.set({
			//     transparentCorners: false,
			//     cornerColor: 'rgba(102,153,255,0.5)',
			//     cornerSize: 12,
			//     padding: 5
			// });
			// var canvas_ex = new fabric.Canvas("canvas_ex");
			// //var canvas_ex = document.getElementById('canvas_ex');
			// canvas_ex.loadFromJSON(json_data, canvas_ex.renderAll.bind(canvas_ex), function(o, object) {
			//     fabric.log(o, object);
			// });
			//canvas.renderAll();
			var dataURL = canvas.toDataURL({
			  format: 'image/png',
			  left: 10,
			  top: 10,
			  width: 450,
			  height: 460
			});
			//var canvas_ex = document.getElementById('canvas_ex');
			
			//var dataURL = canvas_ex.toDataURL();
			
			// let str1 = "data:";
			// let str2 = dataURL;
			// let res = str1.concat(str2);
			console.log(dataURL);
			jQuery("#canvas_export").val(dataURL);
			//jQuery("#ywapo_ctrl_id_5_0").val(dataURL);
			jQuery.post(
             my_ajax_object.ajax_url,
             {
                 'action': 'foobar',
                 'canvas_export': dataURL
             }, 
             function(response) {
                 jQuery("#ywapo_ctrl_id_12_0").val(response);
             }
         );
		});	

		f = fabric.Image.filters;
		canvas.on({
		  	'selection:created': function() {
		      //fabric.util.toArray(document.getElementsByTagName('input')).forEach(function(el){ el.disabled = false; })

		      if(canvas.getActiveObject().get('type')==="Image"){
		      	var filters = ['grayscale', 'invert', 'remove-color', 'sepia', 'brownie',
		      	'brightness', 'contrast', 'saturation', 'noise', 'vintage',
		      	'pixelate', 'blur', 'sharpen', 'emboss', 'technicolor',
		      	'polaroid', 'blend-color', 'gamma', 'kodachrome',
		      	'blackwhite', 'blend-image', 'hue', 'resize'];

		      	for (var i = 0; i < filters.length; i++) {
		      		jQuery(filters[i]) && (
		      			jQuery(filters[i]).checked = !!canvas.getActiveObject().filters[i]);
		      	}
		      }

		  },
		  'selection:cleared': function() {
		      //fabric.util.toArray(document.getElementsByTagName('input')).forEach(function(el){ el.disabled = true; })
		  }
		});
		var webglBackend;
		try {
		webglBackend = new fabric.WebglFilterBackend();
		} catch (e) {
		console.log(e)
		}
		var canvas2dBackend = new fabric.Canvas2dFilterBackend()
  		fabric.filterBackend = fabric.initFilterBackend();
  		fabric.Object.prototype.transparentCorners = false;

		function applyFilter(index, filter) {
			var obj = canvas.getActiveObject();
			obj.filters[index] = filter;
			var timeStart = +new Date();
			obj.applyFilters();
			var timeEnd = +new Date();
			var dimString = canvas.getActiveObject().width + ' x ' +
			canvas.getActiveObject().height;
			//$('bench').innerHTML = dimString + 'px ' +parseFloat(timeEnd-timeStart) + 'ms';
			canvas.renderAll();
		}
		function getFilter(index) {
			var obj = canvas.getActiveObject();
			return obj.filters[index];
		}

		function applyFilterValue(index, prop, value) {
			var obj = canvas.getActiveObject();
			if (obj.filters[index]) {
				obj.filters[index][prop] = value;
				var timeStart = +new Date();
				obj.applyFilters();
				var timeEnd = +new Date();
				var dimString = canvas.getActiveObject().width + ' x ' +
				canvas.getActiveObject().height;
				jQuery('bench').innerHTML = dimString + 'px ' +
				parseFloat(timeEnd-timeStart) + 'ms';
				canvas.renderAll();
			}
		}
		//Apply Filter
		function applyGreyScale(){
			//canvas.setActiveObject(canvas.item(0));
			applyFilter(0, 'true' && new f.Grayscale());
			//#fe6c6a
			// applyFilter(2, 'true' && new f.RemoveColor({
			// 	distance: 0.99,
			// }));
			// applyFilter(16, 'true' && new f.BlendColor({
			// 	color: engraving_color_var,
	  //     		mode: 'overlay',
	  //     		alpha: 0.9
			// }));		
			//applyFilterValue(2, 'distance', 0.6);
			//applyFilterValue(16, 'color', engraving_color_var);
			//applyFilterValue(2, 'color', engraving_color_var);
			// applyFilter(6, 'true' && new f.Contrast({
			// 	contrast: 1,
			// }));
			// applyFilter(5, 'true' && new f.Brightness({
			// 	brightness: -1,
			// }));
			canvas.renderAll();
		} 	 
	//Add Background
	fabric.Image.fromURL(jQuery("#background").val(), function(img) {
		var logo = img.set({ 
			left: 0, 
			top: 0,
			width: canvas.width,
			selectable: false,
			dragable: false,
			evented: false,
			height: canvas.height
		});
		//var engraving_color_var = document.getElementById('engraving_color').value;
		
		// var filter = new fabric.Image.filters.BlendColor({
		//  color: engraving_color_var,
		//  mode: 'overlay',
		//  opacity: 0.3
		// });
		// logo.filters.push(filter);
		// logo.applyFilters();
		
		canvas.add(logo);
		canvas.moveTo(logo, -2);
		logo.setControlsVisibility({
	    	bl: false,
	    	br: false,
	    	tl: false,
	    	tr: false,
	    	mb: false,
	    	ml: false,
	    	mr: false,
	    	mt: false,
	    	mtr: false,
	    });
	    logo.scaleToHeight(460);
	    //logo.scaleTowidth(460);
	    logo.hasRotatingPoint = false;
	    logo.selectable = false;
	    logo.bringForward();
		 logo.bringToFront();
	    canvas.setActiveObject(logo);
		canvas.renderAll();
	});
	//angle controllers
	jQuery("#angle_plus").click(function(){
	  var curAngle = canvas._activeObject.angle;
	  canvas._activeObject.angle = (curAngle+1);
	  jQuery("#angleinput").val(canvas._activeObject.angle); 
	  canvas.renderAll();
	});	
	jQuery("#angle_minus").click(function(){
	  var curAngle = canvas._activeObject.angle;
	  canvas._activeObject.angle = (curAngle-1);
	  jQuery("#angleinput").val(canvas._activeObject.angle); 
	  canvas.renderAll();
	});

	//Zoom controllers
	var defaultscale = 1;
	jQuery("#zoomplus").click(function(){
	  var obj = canvas.getActiveObject();
	  var currentScale = obj.getScaledWidth();
	  var scaleinc = parseFloat(currentScale) + parseInt(50);
   	  obj.scaleToWidth(scaleinc,false);

   	  //Get scale after increment 
   	  var currentScaleInc = obj.getScaledWidth();	
	  jQuery("#zoominput").val(parseInt(currentScaleInc)); 
	  canvas.renderAll();
	});	
	jQuery("#zoomminus").click(function(){
	  var obj = canvas.getActiveObject();
	  var currentScale = obj.getScaledWidth();
	  var scaleinc = parseFloat(currentScale) - parseInt(50);
   	  obj.scaleToWidth(scaleinc,false);

   	  //Get scale after increment 
   	  var currentScaleInc = obj.getScaledWidth();	
	  jQuery("#zoominput").val(parseInt(currentScaleInc)); 
	  canvas.renderAll();
	});

	//Remove Image when clicked
	jQuery(document).on('click', '.single-image .remove-image', function() {
		jQuery(this).parent().remove();
	});



	//Images Uploaded Click Option
	// jQuery(document).on('click', '.upload-image-content img', function() {
	// 	console.log("img click...");
	// 	var src = jQuery(this).attr('src');
	// 	loadImage(src);
	// 	jQuery(".upload-image-button.tool-buttons.main-actions,.upload-image-content").hide();	
	// });
	canvas.on('object:selected', function(event) {
	    var object = event.target;
	    //canvas.sendToBack(object);	    
	    object.sendBackwards();
	    console.log("Selected");
	});	
		// fabric.util.addListener(document.getElementById('icon_right'), 'click', function () {    		
		// 	//var boundingbox = localStorage.getItem("boundingbox");
		//     var obj = canvas.getActiveObject();	    
		//     console.log(obj.getScaledWidth());
		//     obj.set({
		//     	left: boundingbox.left + boundingbox.width - obj.getScaledWidth(),
		//     	selectable: true,
		//     	hasBorders: true,
		//     	hasRotatingPoint: false
		//     });
		//     canvas.setActiveObject(obj);
		//     canvas.renderAll();				
		// });
	//});

	//Default Size and Side B Price
	// var total_price_default = jQuery("#total_price_default").val();
	// var side_b_price = document.getElementById('side_b_price').value; 
	// total_price = parseFloat(40) + parseFloat(side_b_price) + parseFloat(total_price_default);
	// jQuery("#total_price").val(total_price);
	// jQuery(".product-price-container .price .amount:last").html(total_price);
	// jQuery("h3 span#total_price").html(total_price);

	// //Set Selected Size Price on selection
	// jQuery('input[name="select_size"]').on('change', function() {
	// 	var total_price_default = jQuery("#total_price_default").val();
	// 	var selected_size = jQuery(".selsize_value input[type='radio']:checked").val();
	// 	var side_b_price = document.getElementById('side_b_price').value;

	// 	//console.log("selected_size.."+selected_size);
	// 	if (selected_size == "30-40"){
	// 		total_price_size = parseFloat(total_price_default) + parseFloat(40);
	// 	}
	// 	else if (selected_size == "32-22"){
	// 		total_price_size = parseFloat(total_price_default) + parseFloat(50);
	// 	}
	// 	//Adding Side b Price
	// 	total_price_modified = parseFloat(total_price_size) + parseFloat(side_b_price);

	// 	jQuery("#total_price").val(total_price_modified);
	// 	jQuery(".product-price-container .price .amount:last").html(total_price_modified);
	// 	jQuery("h3 span#total_price").html(total_price_modified);
	// });
  // jQuery(".add_to_cart_btn").click(function(){
  //   var total_price = jQuery("#total_price").html();
  //   jQuery.ajax({
  //     url : ajaxurl,
  //     type : 'POST',
  //     data: {
  //       action: 'read_me_later',
  //       total_price : total_price
  //           },
  //     success : function( response ) {
  //       //url = response;
  //       //jQuery("#rt_waiting").css({"display": "none"});
  //       console.log("total_price....Ajax.."+total_price);
  //       //window.open(url,"_self");
  //     },
  //     error: function(e) {
  //         console.log(e);
  //     }
  //   });
  // });
 
 	//Canvas Export
	jQuery( "#download_canvas" ).click(function() {
		
		// var dataURL = canvas.toDataURL({
		//   format: 'png',
		//   left: 10,
		//   top: 10,
		//   width: 450,
		//   height: 460
		// }); 
		// console.log(dataURL);
		// jQuery("#canvastoBlob").attr('src',dataURL);
		// jQuery("#canvas_url").val(dataURL);

		// html2canvas(document.querySelector("#choptoolmain"),{
		// 	allowTaint: true,
		// 	canvas: null
		// }).then(
		// 	canvas => {
		//     document.body.appendChild(canvas)
		// 	}
		// );
		// window.open(canvas.toDataURL('png'));
		
		// html2canvas(document.getElementById('choptoolmain'), {
	 //        allowTaint:true,
	 //        useCORS:true,
	 //        proxy:"lib/html2canvas_proxy/html2canvasproxy.php",
	 //        onrendered: function(canvas) {
	 //            var result = canvas.toDataURL();
	 //            console.log("result.."+result);
	 //        }
	 //   }).then(
		// 	canvas => {
		//     document.body.appendChild(canvas)
		// 	}
		// );


	}); 
	// jQuery( "#download_canvas" ).click(function() {
	//   	function saveImage(e) {
	// 	    this.href = canvas1.toDataURL({
	// 	        format: 'png',
	// 	        quality: 0.8
	// 	    });
	// 	    this.download = 'design.png'
	// 	}
	// });
  //Export Fabric Canvas to Image
  // fabric.Image.fromURL('https://via.placeholder.com/350x150', function(img){
  //   img.setWidth(200);
  //   img.setHeight(200);
  //   canvas.add(img);
  // }); 
  //const client = filestack.init('AeAn2tWnEREO3FZeI6R9yz');
  //client.picker().open();

	// jQuery(".add_to_cart_btn.tool-buttons-text").click(function(){
 //    //jQuery(".icon-box.featured-box.total_nis_btn.icon-box-right.text-right").click(function(){
 //    	jQuery("#canvas").get(0).toBlob(function(blob){
 //      saveAs(blob, "myIMG.png");
 //      console.log(blob);
      
 //      var reader = new FileReader();
 //      reader.readAsDataURL(blob); 
 //      reader.onloadend = function() {
 //      	var base64data = reader.result;                
 //      	console.log(base64data);
 //      	jQuery("#canvastoBlob").attr("src", base64data);
 //      	jQuery("#canvas_export").val(base64data);
 //      }
 //  	});    
 //  });    

    //}); 

	    //Load Uploaded Image into Canvas
		 //var src1 = "https://cdn.filestackcontent.com/889uTzOERFujn9CAUmnd";
		 var uploaded_file = localStorage.getItem("uploaded_file");
		 //loadImage(uploaded_file);
		 jQuery(".upload-image-button").click(function(){
				const apikey = 'AYGP0ayqQSLSakKdifkwhz';
				const client = filestack.init(apikey);
				const options = {
					maxFiles: 20,
					maxSize: 3000000,
					uploadInBackground: false,
					onOpen: () => console.log('opened!'),
					onUploadDone: (res) => addtocanvas(res),
				};
			//localStorage.setItem("uploaded_file", res);
			client.picker(options).open();
		});   
	
		//Background Color and Image 
		var use_image_background = jQuery("#use_image_background").val();
		console.log("use_image_background.."+use_image_background);
		if (use_image_background == "Yes") {
			var container_background_image = jQuery("#container_background_image").val();
	   	console.log("container_background_image.."+container_background_image);
	   	jQuery(".canvas-container").css('background-image', 'url(' + container_background_image + ')');
		}else{
			var container_background_color = jQuery("#container_background_color").val();
	   	jQuery(".canvas-container").css("background", container_background_color);
    	}
		function addtocanvas(res){
			//console.log(res);
			console.log("Uploaded File.."+res.filesUploaded[0].url);
			console.log("Uploaded FileName.."+res.filesUploaded[0].filename);
			console.log("Uploaded Key.."+res.filesUploaded[0].Key);
			console.log("Uploaded Mime Type.."+res.filesUploaded[0].mimetype);
			//localStorage.setItem("uploaded_file", res.filesUploaded[0].url);
			var uploaded_file = res.filesUploaded[0].url;
			loadImage(uploaded_file);
			jQuery(".main-actions-parent").hide();
			// jQuery.post(
   //              my_ajax_object.ajax_url, 
   //              {
   //                  'action': 'foobar',
   //                  'clicked_image': uploaded_file
   //              }, 
   //              function(response) {
   //                  console.log('The server responded: ', response);
   //                  jQuery("#preview").attr("src","data:image/png;base64,"+response);
   //              }
   //          );
		jQuery("#zoomplus,#zoomminus,#angle_plus,#angle_minus").prop('disabled', false);
		}

});